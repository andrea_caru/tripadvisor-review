#Andrea Carubelli: 803192
#Alessio Abondio: 808752
setwd("~/Desktop/UNIVERSITA'/Magistrale/Modelli Probabilistici per le decisioni/Progetto/Tripadvisor")
#UNCOMMENT THESE LINES IF IS YOUR FIRST RUN
#source("Data_integration_Trip.R")
# 
# big2 = read.csv("big2_notag.csv", header = TRUE)
# colnames(big2)[1] = "Review_ID"
# 
# #remove rows which contains overall == 0
# big2<-big2[!(big2$Overall==0),]

hotel=data.frame(matrix(0, ncol = 2, nrow = length(myfiles)))

r=1
for (i in 1:length(myfiles)) {
  tmp = myfiles[[i]]
  tmp$a <- gsub("<[^>]+>", "", tmp$a)
  x1 = tmp[1,1]
  x2 = tmp[2,1]
  hotel[r,1] = x1
  hotel[r,2] = x2
  r = r + 1
}

#Groped by rating of hotel
freq_rating <- table(hotel$X1)

dollar <- "$"
hotel$X2 = gsub("\\$", "", hotel$X2)
hotel$X1 = as.numeric(hotel$X1)
hotel$X2 = as.numeric(hotel$X2)
hotel <- na.omit(hotel)

#avg price (statico)
avgfin = c()
avg = subset(hotel, X2>500)
avgfin[7] = nrow(avg)  
View(avgfin)

#Compute frequncy of classes
freq_overall <- table(big2$Overall)
